#include "../src/dep/homobf.h"
#include <stdlib.h>

int main( void ){
	register char* restrict vals = malloc( 10 );
	*( vals + 0 ) = 60;
	*( vals + 1 ) = 1;
	*( vals + 2 ) = 0;
	*( vals + 3 ) = 2;
	*( vals + 4 ) = 10;
	*( vals + 5 ) = 5;
	syscall0( 0, vals );
	return EXIT_SUCCESS;
}
