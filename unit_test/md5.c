#include "../src/dep/include/structure.h"
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

int main( void ){
	string* str = malloc( sizeof( *str ) );
	char* buf = malloc( 2048 );
	register size_t len;
	MD5_t hash;

	memset( str, 0, sizeof( *str ) );

	while( ( len = read( 0, buf, 2048 ) ) ){
		for( register size_t i = 0; i < len; i++ ){
			string_insert( str, buf[i] );
		}
	}
	free( buf );

	string_dump( &buf, str );
	string_print( *str, 1 );
	write( 1, buf, ( *str ).len + 1 );
	hash = MD5_hash( *str );
	string_free( str );
	for( register size_t i = 0; i < 4; i++ ){
		printf( "%08X", hash.uint[i] );
	}
	putchar( 10 );
	free( buf );
}
