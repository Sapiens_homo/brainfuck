#include <stdio.h>
#include <stdlib.h>
#include "structure.h"

int main( void ){
	void* l = list_alloc();
	void* s = stack_alloc();
	unsigned char t;
	void* tmp;
	unsigned char* a = malloc( sizeof( *a ) * 5 );
	for( register unsigned char i = 0; i < 5; i++ ){
		*( a + i ) = i*i+1;
		list_append( l, *( a + i ) );
		stack_push( s, a + i );
	}
	while( ( t = list_pop( l ) ) ){
		printf( "%hhu ", t );
	}
	printf("\n");
	while( ( tmp = stack_pop( s ) ) ){
		printf( "%d ", *( __typeof__(a) )tmp );
	}
	fclose( stdin );
	list_free( l );
	stack_free( s );
	free( a );
}
