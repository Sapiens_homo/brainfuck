
#ifndef __STRUCTURE_H__
#define __STRUCTURE_H__

#include <stdlib.h>

#include "structure/arr.h"
#include "structure/table.h"
#include "structure/func.h"
#include "structure/bst.h"

extern size_t MEM_SIZE;

#endif
