#ifndef __HOMO_BF_H__
#define __HOMO_BF_H__

#include <stdlib.h>
#include "structure.h"
#include "error.h"

enum __HOMOBF_ERR__ syntax_check( char *restrict, size_t );

struct _FUNCS preprocessor( char *restrict, size_t );

int interact( void );

int intepreter( const struct _FUNCS func, arr_t* restrict buf );

void bf_syscall( char *, size_t, size_t ); /* ptr, current position, memory ring length */
void bf_socketcall( char *, size_t, size_t ); /* same as syscall */

size_t get_content( const char* restrict, char ** );

#endif
