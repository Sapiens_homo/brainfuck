
#ifndef __STRUCTURE_ARR_H__
#define __STRUCTURE_ARR_H__

#include <stdlib.h>

struct _ARR {
	size_t len;
	union {
		char *d;
		unsigned char *u;
	};
	char *ptr;
};

typedef struct _ARR arr_t;

#endif
