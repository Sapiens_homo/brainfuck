
#ifndef __STRUCTURE_FUNC_H__
#define __STRUCTURE_FUNC_H__

#include <stdlib.h>

#include "table.h"

struct _FUNC {
	char *main;
	size_t len;
	size_t fns;
	union {
		struct _FUNC** fn;
		char **hashs;
	};
	struct _TABLE table;
};

struct _FUNCS {
	size_t len;
	struct _FUNC **fns;
};

void FN_free ( struct _FUNCS );

typedef struct _FUNCS fn;

#endif
