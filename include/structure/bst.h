
#ifndef __STRUCTURE_BST_H__
#define __STRUCTURE_BST_H__

struct _BST_node {
	char *hash;
	struct _BST_node *left;
	struct _BST_node *right;
	void *data;
};

void *BST_search ( struct _BST_node *, char * );
void BST_insert ( struct _BST_node **, char *, void * );
void BST_free ( struct _BST_node * );
/*
 * free nodes, hashes
 * do not free data
 */

typedef struct _BST_node BST_t;

#endif
