
#ifndef __STRUCTURE_TABLE_H__
#define __STRUCTURE_TABLE_H__

#include <stdlib.h>

struct _TAB {
	size_t former;
	size_t latter;
};

struct _TABLE {
	size_t len;
	struct _TAB *tabs;
	struct _TAB *revs;
};

struct _TABLE table_construct ( const char *restrict, const size_t );
void table_free( struct _TABLE * );

typedef struct _TABLE table_t;

#endif
