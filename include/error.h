
#ifndef __ERROR_H__
#define __ERROR_H__

enum __HOMOBF_ERR__ {
	HOMOBF_SUCCESS = 0,
	HOMOBF_UNCLOSED_BRACKETS,
	HOMOBF_EARLY_EOF,
	HOMOBF_FN_NOT_FOUND,
};

#define PERROR( e ) __print_error__( e )
#define __ERR__( e ) case e:\
	fprintf( stderr, "%s: %s: %d: %s", __FILE__, __func__, __LINE__, #e );\
	break
#define __print_error__( e ) __extension__({\
		switch ( e ){\
		__ERR__(HOMOBF_SUCCESS);\
		__ERR__(HOMOBF_UNCLOSED_BRACKETS);\
		__ERR__(HOMOBF_EARLY_EOF);\
		__ERR__(HOMOBF_FN_NOT_FOUND);\
		}\
		})

#endif
