BrainFuck Interpreter
=======================
version 2.0.0

## Overview

### Standard Brainfuck
```bf
+ increase
- decrease
> move right
< move left
[ loop beginning
] loop end
. output to stdout
, read from stdin
```

### Extension
```bf
# make a socket call
% make a syscall
@ call function ( exit while read tape from stdin )
$ Preprocessor
```
**All the number is Big-endian**

## System call

the pointing cell, called `cell 0`, and the next 3 cells store the number of the system call. ( as an 32bit Big-endian unsigned integer )
the next cell, `cell 4`, is the count of arguments.
`cell 5` ~: arguments
result: will fill cell 0, cell 1, ... in the endian depending on your hardware.

There are 3 types of arguments, normal, contents pointer, and pointer to cell.

## Functions

- `cell 0`: number of the function
***Calling a function won't reset the memory, just like fork***
The returning value:
- 'cell 0': length of value
The returning value will be copy to caller's memory tape from the position it called the function.

## Preprocessor

To unify the number of function calls, we introduced a C-like preprocessor.
for example, you wrote a function f in header.h, and you want to use it in main.c
```c
/* main.c */
#include "header.h"
```

as C, we can do same thing in bf
```bf
$++++++++++[>++++++++++<-]>[-<+>]<[->+>+>+>+>+>+<<<<<<]>+>--->>+>++++++++++++++>[-<<<<<<+>>>>>>]<<<<<<++++
```
equal to `$header` in plain text, to include "header" as a function tape.

The functions' index are count from 1 because function 0 is the tape itself.

## SocketCall
 
Equal to
```c
int syscall( SYS\_socketcall, `Cell 0`, `Cell 1~` );
```
on some architecture of Linux kernels.
- `Cell 0`: SocketCall number
    returning value: `Cell 0~`

if the argument is a pointer, address of the beginning cell will be passed to and it'll be the last argument.
E.g. recv( int, void*, size_t, int );
cells should contain ( int, size_t, int, ... ).

## Usage
 **The source code was written in *GNU99* standard**
### Build

```sh
make
```
or
```sh
make all
```

### Install

```sh
sudo make install
```
will be install to `/opt/homobf` and add 2 files to `/etc/profile.d`

### Use

#### Interactive interface
```sh
./homobf #without installing
```
```sh
homobf #installed
```

#### Interpreter
```sh
./homobf maintape.bf #without installing
```
```sh
homobf maintape.bf
```

## Reference

- [systemf by Yoon](https://github.com/ajyoon/systemf)
