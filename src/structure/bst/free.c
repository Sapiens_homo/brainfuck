#include "../../../include/structure.h"

void BST_free( BST_t *tree ){
	struct L {
		struct L *next;
		BST_t *data;
	};
	register struct L *stack;
	register struct L *tmp;
	register struct L *ch;
	if ( tree ){
		stack = malloc( sizeof( *stack ) );
		( *stack ).next = NULL;
		( *stack ).data = tree;
		while ( stack ){
			tmp = stack;
			stack = ( *stack ).next;
			if ( ( *( *tmp ).data ).left ){
				ch = malloc( sizeof( *ch ) );
				( *ch ).data = ( *( *tmp ).data ).left;
				( *ch ).next = stack;
				stack = ch;
			}
			if ( ( *( *tmp ).data ).right ){
				ch = malloc( sizeof( *ch ) );
				( *ch ).data = ( *( *tmp ).data ).right;
				( *ch ).next = stack;
				stack = ch;
			}

			free( ( *tmp ).data );
			free( tmp );
		}
	}
}

/*
void BST_free( BST_t *tree ){
	BST_free( ( *tree ).left );
	BST_free( ( *tree ).right );

	free( tree );
}
*/
