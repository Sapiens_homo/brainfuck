#include "../../../include/structure.h"
#include "../../../hcl/hcl.h"

void *BST_search( BST_t *tree, char *key ){
	register void *ret = NULL;
	register int res;
	while( tree ){
		res = MD5_cmp( key, ( *tree ).hash );
		if ( !res ){
			ret = ( *tree ).data;
			break;
		}else if ( res < 0 ){
			tree = ( *tree ).left;
		}else{
			tree = ( *tree ).right;
		}
	}
	return ret;
}
