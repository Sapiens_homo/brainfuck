#include "../../../include/structure.h"
#include "../../../hcl/hcl.h"

#include <string.h>

void BST_insert( BST_t **tree, char *key, void *value ){
	register struct _BST_node** now = NULL;
	register int res;

	if( !tree ){
		return;
	}

	now = tree;
	while( *now ){
		res = MD5_cmp( key, ( **now ).hash );
		if( res < 0 ){
			now = &( **now ).left;
		}else{
			now = &( **now ).right;
		}
	}

	*now = malloc( sizeof( **now ) );
	memset( *now, 0, sizeof( **now ) );
	( **now ).hash = key;
	( **now ).data = value;
}
