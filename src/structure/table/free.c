#include "../../../include/structure.h"

void table_free( table_t* table ){
	free( ( *table ).tabs );
	free( ( *table ).revs );
}
