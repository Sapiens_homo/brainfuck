#include "../../../include/structure.h"

#include <string.h>

int table_cmp0( const void *, const void * );
int table_cmp1( const void *, const void * );

struct _node{
	struct _node *next;
	struct _TAB *tab;
};

table_t table_construct( const char* restrict str, const size_t len ){
	register table_t ret = {
		.len = 0L,
		.tabs = NULL,
		.revs = NULL
	};

	register struct _node* stack;
	register struct _node *lst;

	register struct _node *tt;

	register size_t i = 0;
	register struct _TAB *tmp;

	stack = lst = tt = NULL;

	while( i < len ){
		switch( *( str + i ) ){
			case '[':
				tmp = malloc( sizeof( *tmp ) );
				( *tmp ).former = i;
				tt = malloc( sizeof( *tt ) );
				( *tt ).tab = tmp;
				( *tt ).next = stack;
				stack = tt;
				break;
			case ']':
				tt = ( *stack ).next;
				tmp = ( *stack ).tab;
				( *tmp ).latter = i;
				free( stack );
				stack = tt;
				tt = malloc( sizeof( *tt ) );
				( *tt ).next = lst;
				( *tt ).tab = tmp;
				lst = tt;
				ret.len++;
				break;
			default:
				break;
		}
		i++;
	}

	ret.tabs = malloc( sizeof( *ret.tabs ) * ret.len );
	ret.revs = malloc( sizeof( *ret.revs ) * ret.len );

	i = 0;
	while( lst ){
		tt = ( *lst ).next;
		tmp = ( *lst ).tab;
		memcpy( ret.tabs + i, tmp, sizeof( *ret.tabs ) );
		free( tmp );
		free( lst );
		lst = tt;
		i++;
	}

	memcpy( ret.revs, ret.tabs, sizeof( *ret.tabs ) * ret.len  );
	qsort( ret.tabs, ret.len, sizeof( *ret.tabs ), table_cmp0 );
	qsort( ret.revs, ret.len, sizeof( *ret.revs ), table_cmp1 );

	return ret;
}

int table_cmp0( const void *a, const void *b ){
	const register struct _TAB *c = a;
	const register struct _TAB *d = b;
	
	return ( ( *c ).former > ( *d ).former ) - ( ( *c ).former < ( *d ).former );
}

int table_cmp1( const void *a, const void *b ){
	const register struct _TAB *c = a;
	const register struct _TAB *d = b;
	
	return ( ( *c ).latter > ( *d ).latter ) - ( ( *c ).latter < ( *d ).latter );
}
