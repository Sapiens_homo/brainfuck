#include "../include/brainfuck.h"
#include "../include/error.h"

#include "../hcl/hcl.h"

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

size_t MEM_SIZE = 3192L;

int main( int argc, char** argv ){
	register int opt;

	register fn funcs = {
		.len = 0,
		.fns = NULL
	};

	while( ( opt = getopt( argc, argv, "m:" ) ) != -1 ){
		switch( opt ){
			case 'm':
				MEM_SIZE = atol( optarg );
				break;
			default:
				fprintf( stderr, "Usage: %s [-m byte] [main tape]\n", argv[0] );
				break;
		}
	}

	if( optind >= argc ){
		return interact();
	}

	funcs = preprocessor( argv[ optind ], 0 );

	intepreter( funcs, NULL );

	FN_free ( funcs );

	return EXIT_SUCCESS;
}
