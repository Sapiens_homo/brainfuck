; System caller, amd64 Linux
.global bf_syscall
	.text

bf_syscall:
; arr_t* r11
	push %rbp
	movq %rsp, %rbp

	movq %rdi, %r11
	mov 0[%r11], %rax

	pop %rbp
	ret
