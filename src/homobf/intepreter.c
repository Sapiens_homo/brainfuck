#include "../../include/homobf.h"

#include <stdio.h>
#include <string.h>

#include <errno.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <limits.h>

#ifndef true
#define true ( 69 )
#endif

size_t MEM_SIZE = 3072L;

size_t get_num( char *, size_t );

int intepreter( const struct _FUNCS func, arr_t* restrict buf ){

	register size_t left, right, mid, reg;
	register int status = EXIT_SUCCESS;

	register struct call_stack {
		struct call_stack *next;
		arr_t *mem;
		char *pc;
		char *end; /* fn.len + fn.main */
		struct _FUNC *fn;
		char save; /* true - not be freed */
	} *cs;
	register void *tmp;
	register void *tmp1;

	cs = malloc( sizeof( *cs ) );

	if ( buf ){
		( *cs ).mem = buf;
		( *cs ).save = 69;
	}else{
MLC0:
		if ( !( ( *cs ).mem = malloc( sizeof( *( *cs ).mem ) ) ) ){
			goto MLC0;
		}
		( *( *cs ).mem ).len = MEM_SIZE;
MLC1:
		if ( !( ( *( *cs ).mem ).d = malloc( ( *( *cs ).mem ).len ) ) ){
			goto MLC1;
		}
		memset( ( *( *cs ).mem ).d, 0, ( *( *cs ).mem ).len );
		( *( *cs ).mem ).ptr = ( *( *cs ).mem ).d;
		( *cs ).save = 0;
	}

	( *cs ).next = NULL;
	( *cs ).pc = ( *( *func.fns ) ).main;
	( *cs ).end = ( *( *func.fns ) ).main + ( *( *func.fns ) ).len;
	( *cs ).fn = *func.fns;

	while ( cs ){
		switch ( *( *cs ).pc ){

			case '+':
				( *( *( *cs ).mem ).ptr ) += 1;
				break;
			case '-':
				( *( *( *cs ).mem ).ptr ) -= 1;
				break;
			case '>':
				( *( *cs ).mem ).ptr += (
					( *( *cs ).mem ).ptr
					- ( *( *cs ).mem ).d
					+ ( *( *cs ).mem ).len
					+ 1
				) % ( *( *cs ).mem ).len;
				break;
			case '<':
				( *( *cs ).mem ).ptr += (
					( *( *cs ).mem ).ptr
					- ( *( *cs ).mem ).d
					+ ( *( *cs ).mem ).len
					- 1
				) % ( *( *cs ).mem ).len;
				break;
			case '.':
				syscall( SYS_write, 1, ( *( *cs ).mem ).ptr, 1 );
				break;
			case ',':
				syscall( SYS_read, 0, ( *( *cs ).mem ).ptr, 1 );
				break;
			case '[':
				if ( !( *( *( *cs ).mem ).ptr ) ){
					left = 0;
					right = ( *( *cs ).fn ).table.len;
					while ( left < right ){
						mid = ( ( right - left ) >> 1 ) + left;
						reg = ( *cs ).pc - ( *( *cs ).fn ).main;
						if ( !( ( *( ( *( *cs ).fn ).table.tabs + mid ) ).former - reg ) ){
							break;
						}else if ( ( *( ( *( *cs ).fn ).table.tabs + mid ) ).former > reg ){
							right = mid;
						}else{
							left = mid + 1;
						}
					}
					( *cs ).pc = ( *( *cs ).fn ).main + ( *( ( *( *cs ).fn ).table.tabs + mid ) ).latter;
				}
				break;
			case ']':
				left = 0;
				right = ( *( *cs ).fn ).table.len;
				while ( left < right ){
					mid = ( ( right - left ) >> 1 ) + left;
					reg = ( *cs ).pc - ( *( *cs ).fn ).main;
					if ( !( ( *( ( *( *cs ).fn ).table.revs + mid ) ).former - reg ) ){
						break;
					}else if ( ( *( ( *( *cs ).fn ).table.revs + mid ) ).former > reg ){
						right = mid;
					}else{
						left = mid + 1;
					}
				}
				( *cs ).pc = ( *( *cs ).fn ).main + ( *( ( *( *cs ).fn ).table.revs + mid ) ).latter;
				break;

			case '@':
				if ( !*( *( *cs ).mem ).ptr ){
					break;
				}
				/* push cs */
MLC2:
				if ( !( tmp = malloc( sizeof( *cs ) ) ) ){
					goto MLC2;
				}
				memset( tmp, 0, sizeof( *cs ) );
				( *( __typeof__( cs ) )tmp ).next = cs;
MLC3:
				if ( !( ( *( __typeof__( cs ) )tmp ).mem = malloc( sizeof( *( *( __typeof__( cs ) )tmp ).mem ) ) ) ){
					goto MLC3;
				}
				( *( *( __typeof__( cs ) )tmp ).mem ).len = MEM_SIZE;
MLC4:
				if ( !( ( *( *( __typeof__( cs ) )tmp ).mem ).d = malloc( ( *( *( __typeof__( cs ) )tmp ).mem ).len ) ) ){
					goto MLC4;
				}
				memset( ( *( *( __typeof__( cs ) )tmp ).mem ).d, 0, ( *( *( __typeof__( cs ) )tmp ).mem ).len );
				( *( *( __typeof__( cs ) )tmp ).mem ).ptr = ( *( *( __typeof__( cs ) )tmp ).mem ).d;
				( *( __typeof__( cs ) )tmp ).fn = *( ( *( *cs ).fn ).fn + *( *( *cs ).mem ).ptr );
				( *( __typeof__( cs ) )tmp ).pc = ( *( *( __typeof__( cs ) )tmp ).fn ).main;
				( *( __typeof__( cs ) )tmp ).end = ( *( __typeof__( cs ) )tmp ).pc + ( *( *( __typeof__( cs ) )tmp ).fn ).len;

				memcpy( ( *( *( __typeof__( cs ) )tmp ).mem ).d, ( *( *cs ).mem ).d, MEM_SIZE );

				cs = tmp;
				break;
			case '#':
				bf_socketcall( ( *( *cs ).mem ).ptr, ( *( *cs ).mem ).ptr - ( *( *cs ).mem ).d, ( *( *cs ).mem ).len );
				break;
			case '%':
				bf_syscall( ( *( *cs ).mem ).ptr, ( *( *cs ).mem ).ptr - ( *( *cs ).mem ).d, ( *( *cs ).mem ).len );
				break;
			case '^':
				if ( *( *( *cs ).mem ).ptr ){
					if ( *( *( *cs ).mem ).ptr < 0 ){
						tmp1 = ( void * )( size_t )( ~*( *( *cs ).mem ).ptr + 1 );
						tmp = ( void * )1;
					}else{
						tmp1 = ( void * )( size_t )( *( *( *cs ).mem ).ptr );
						tmp = NULL;
					}
					if (
						( ( void * )( ( *cs ).pc - ( *( *cs ).fn ).main ) < tmp1 && tmp ) ||
						( ( void * )( ( *( *cs ).fn ).len - ( ( *cs ).pc - ( *( *cs ).fn ).main ) ) < tmp1 && tmp )
					){
						( *cs ).pc += ( tmp )?( ~( size_t )tmp1 + 1 ):( ( size_t )tmp1 );
					}else{
						status = EXIT_FAILURE;
						errno = ERANGE;
						goto ERR;
					}
				}
				break;
		}
		( *cs ).pc++;
		if ( ( *cs ).pc >= ( *cs ).end ){
			tmp = ( *cs ).next;
			if ( tmp ){
MLC5:
				if ( !( tmp1 = malloc( UCHAR_MAX + 1 ) ) ){
					goto MLC5;
				}

				if (
					( size_t )*( *( *cs ).mem ).ptr >=
					MEM_SIZE - ( ( *( *cs ).mem ).ptr - ( *( *cs ).mem ).d )
				){
					memcpy(
						tmp1,
						( *( *cs ).mem ).ptr,
						MEM_SIZE - ( 1 + *( *( *cs ).mem ).ptr - ( ( *( *cs ).mem ).ptr - ( *( *cs ).mem ).d ) )
					);
					memcpy(
						tmp1 +
						MEM_SIZE - ( 1 + *( *( *cs ).mem ).ptr - ( ( *( *cs ).mem ).ptr - ( *( *cs ).mem ).d ) ),
						( *( *cs ).mem ).d,
						( 1 + *( *( *cs ).mem ).ptr - ( ( *( *cs ).mem ).ptr - ( *( *cs ).mem ).d ) )
					);

				}else{
					memcpy( tmp1, ( *( *cs ).mem ).ptr, 1 + ( size_t )*( *( *cs ).mem ).ptr );
				}

				if (
					( size_t )*( *( *cs ).mem ).ptr >=
					MEM_SIZE - ( ( *( *( __typeof__( cs ) )tmp ).mem ).ptr - ( *( *( __typeof__( cs ) )tmp ).mem ).d )
				){
					memcpy(
						( *( *( __typeof__( cs ) )tmp ).mem ).ptr,
						tmp1,
						MEM_SIZE - 1 - ( ( *( *( __typeof__( cs ) )tmp ).mem ).ptr - ( *( *( __typeof__( cs ) )tmp ).mem ).d )
					);
					memcpy(
						( *( *( __typeof__( cs ) )tmp ).mem ).d,
						tmp1 +
						MEM_SIZE - 1 - ( ( *( *( __typeof__( cs ) )tmp ).mem ).ptr - ( *( *( __typeof__( cs ) )tmp ).mem ).d ),
						1 + ( ( *( *( __typeof__( cs ) )tmp ).mem ).ptr - ( *( *( __typeof__( cs ) )tmp ).mem ).d )
					);
				}else{
					memcpy( ( *( *( __typeof__( cs ) )tmp ).mem ).ptr, tmp1, 1 + ( size_t )*( *( *cs ).mem ).ptr );
				}

				free( tmp1 );
			}
			if ( !( *cs ).save ){
				free( ( *( *cs ).mem ).d );
				free( ( *cs ).mem );
			}

			free( cs );
			cs = tmp;
		}
	}

	return status;
ERR:
	while ( cs ){
		if ( !( *cs ).save ){
			free( ( *( *cs ).mem ).d );
			free( ( *cs ).mem );
		}

		tmp = ( *cs ).next;
		free( cs );
		cs = tmp;
	}
	return status;
}
