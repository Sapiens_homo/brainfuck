#include "../../include/homobf.h"

enum __HOMOBF_ERR__ syntax_check( char *restrict str, size_t len ){
	register size_t lft = 0;
	register size_t i;
	for (  i = 0; i < len; i++ ){
		switch( *( str + len ) ){
			case '[':
				lft++;
				break;
			case ']':
				if( lft ){
					lft--;
				}else{
					return HOMOBF_UNCLOSED_BRACKETS;
				}
				break;
			default:
				break;
		}
	}
	return lft > 0;
}
