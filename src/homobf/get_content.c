#include "../../include/homobf.h"

#include <sys/syscall.h>
#include <unistd.h>

#include <fcntl.h>
#include <string.h>

struct L {
	struct L *next;
	void *t;
};

size_t get_content( const char* restrict file, char **ret ){
	register int fd;
	register size_t len;
	register size_t got;
	register char *buf;
	register void *tmp;
	register struct L *list;

	if ( file ){
		if( ( fd = syscall( SYS_open, file, 0 ) ) <= 0 ){
			goto FAIL;
		}

		if(
			( len = syscall( SYS_lseek, fd, 0, SEEK_END ) ) ==
			( __typeof__( len ) )( off_t )( -1L )
		){
			goto FAIL;
		}

MLC0:
		if( !( *ret = malloc( sizeof( **ret ) * len ) ) ){
			goto MLC0;
		}
		syscall( SYS_lseek, fd, 0, SEEK_SET );
		syscall( SYS_read, fd, *ret, len );
		syscall( SYS_close, fd );
	}else{
		fd = 0;
		buf = malloc( 256 );
		len = 0;
		got = 0;
		memset( buf, 0, 256 );
		list = NULL;

		while ( ( got = read( fd, buf, 256 ) ) > 0 ){
			len += got;
			tmp = malloc( sizeof( *list ) );
			( *( __typeof__( list ) )tmp ).next = list;
MLC1:
			if ( !( ( *( __typeof__( list ) )tmp ).t = malloc( 256 ) ) ){
				goto MLC1;
			}
			list = tmp;
			memcpy( ( *list ).t, buf, got );
		}

MLC2:
		if ( !( *ret = malloc( len ) ) ){
			goto MLC2;
		}
		got = len;
		
		while ( got > 256 ){
			got -= 256;
			memcpy ( *ret + got, ( *list ).t, 256 );
			tmp = ( *list ).next;
			free( ( *list ).t );
			free( list );
			list = tmp;
		}

		memcpy( *ret, ( *list ).t, got );
		free( ( *list ).t );
		free( list );

		free( buf );
	}

	return len;
FAIL:
	*ret = NULL;
	return 0;
}
