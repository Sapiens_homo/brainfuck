#include "../../include/homobf.h"

#include <string.h>

struct L {
	struct L *next;
	void *t;
};

int interact( void ){
	register int status = EXIT_SUCCESS;
	register int returning = 0;
	register int END = 0;
	register fn fun;
	char *script;
	register size_t script_len;
	register arr_t *mem = NULL;

MLC0:
	if ( !( mem = malloc( sizeof( *mem ) ) ) ){
		goto MLC0;
	}
MLC1:
	( *mem ).len = MEM_SIZE;
	if ( !( ( *mem ).d = malloc( MEM_SIZE ) ) ){
		goto MLC1;
	}
	( *mem ).ptr = ( *mem ).d;
	while( returning == EXIT_SUCCESS ){
		script_len = get_content( NULL, &script );
		fun = preprocessor( script, script_len );
		status = intepreter( fun, mem );
		if( END ){
			break;
		}
	}
	free( ( *mem ).d );
	free( mem );

	return status;
}
