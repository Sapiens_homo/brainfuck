#include "../../include/homobf.h"
#include "../../hcl/hcl.h"
#include "../../include/error.h"

#include <stdio.h>

#include <string.h>

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>

#include <errno.h>

#if _POSIX_VERSION < 200809L || _POSIX_C_SOURCE < 200809L
#error "Min POSIX version required: POSIX.1-2008"
#endif

extern size_t MEM_SIZE;

struct node {
	struct node *next;
	void *data;
	char *cnt;
	size_t len;
};

struct L {
	struct L *next;
	void *d;
};

struct M {
	struct M *next;
	void *d;
	void *h;
};

char *get_dir ( char *restrict );
char *join_path ( char *restrict, char *restrict );

struct _FUNCS preprocessor ( char *restrict main, size_t len ){
	register struct _FUNCS ret = {};
	register size_t i, ii;
	register void *tmp;
	register char *pwd;
	
	register char *mem;
	register char *mem_ptr;
	register char *pc;
	register char *cnt;
	register size_t mem_bias;
	register char rec;

	register char *hash;

	register enum __HOMOBF_ERR__ err;

	char *content;

	BST_t *root = NULL;
	struct node *stack = NULL;
	register struct L *fns = NULL;
	register struct _FUNC *FN;
	register size_t FNS;
	register struct M *fn_save;

	if ( !main ){
		goto RET;
	}

	stack = malloc( sizeof( *stack ) );
	( *stack ).next = NULL;
	if ( len ){
		content = malloc( len );
		memcpy( content, main, len );
		( *stack ).data = malloc(1);
		*( char* )( *stack ).data = 0;
	}else{
		len = get_content( main, &content );
		( *stack ).data = join_path( NULL, main );
	}
	( *stack ).cnt = content;
	( *stack ).len = len;
	mem = malloc( MEM_SIZE );
	fn_save = NULL;

	while ( stack ){
		pwd = get_dir( ( *stack ).data );
		cnt = pc = ( *stack ).cnt;
		len = ( *stack ).len;
		tmp = ( *stack ).next;
		free( stack );
		stack = tmp;

		if ( ( err = syntax_check( pc, len ) ) ){
			PERROR(err);
			goto RET;
		}

		memset( mem, 0, MEM_SIZE );
		mem_ptr = mem;
		mem_bias = 0;
		rec = 0;
		FNS = 0;
		fns = NULL;
		while ( pc - cnt < len ){
			switch ( *pc ){
				case '$':
					if ( rec ){
					}
					rec = 1;
					break;
				case 10:
					if ( rec ){
						/* push new header into stack */
						tmp = malloc( sizeof( *stack ) );
						( *( __typeof__( stack ) )tmp ).next = stack;
						( *( __typeof__( stack ) )tmp ).data = join_path( pwd, mem );
						( *( __typeof__( stack ) )tmp ).len = get_content( ( *( __typeof__( stack ) )tmp ).data, &content );
						( *( __typeof__( stack ) )tmp ).cnt = content;
						stack = tmp;
						FNS++;
						tmp = malloc( sizeof( *fns ) );
						hash = MD5_hash( ( *stack ).cnt, ( *stack ).len );
						( *( __typeof__( fns ) )tmp ).d = hash;
						( *( __typeof__( fns ) )tmp ).next = fns;
						fns = tmp;

						rec = 0;
						memset( mem, 0, MEM_SIZE );
						mem_ptr = mem;
						mem_bias = 0;
					}else{
						pc++;
						goto PROC;
					}
					break;
				case '[':
					if ( !*mem_ptr ){
						for ( ; *(pc++) - ']'; );
						continue;
					}
					break;
				case ']':
					for ( ; *(pc--) - '['; );
					break;
				case '>':
					mem_ptr++;
					mem_bias++;
					if ( !( mem_bias - MEM_SIZE ) ){
						mem_ptr = mem;
						mem_bias = 0;
					}
					break;
				case '<':
					mem_ptr--;
					if ( !mem_bias ){
						mem_bias = MEM_SIZE - 1;
						mem_ptr = mem + mem_bias;
					}
					break;
				case '+':
					( *mem_ptr )++;
					break;
				case '-':
					( *mem_ptr )--;
					break;
			}
			( *pc )++;
		}

PROC:
		free( pwd );

		hash = MD5_hash( cnt, len );
		len = pc - cnt;
		tmp = malloc( len );
		memcpy( tmp, pc, len );
		free( cnt );
		cnt = tmp;
		if ( !BST_search( root, hash ) ){
			FN = malloc( sizeof( *FN ) );
			( *FN ).main = cnt;
			( *FN ).len = len;
			( *FN ).hashs = malloc( sizeof( hash ) * FNS );
			( *FN ).fns = FNS;
			while ( FNS-- ){
				*( ( *FN ).hashs + FNS ) = ( *fns ).d;
				tmp = ( *fns ).next;
				free( fns );
				fns = tmp;
			}
			( *FN ).table = table_construct( cnt, len );
			BST_insert( &root, hash, FN );
			
			tmp = malloc( sizeof( *fn_save ) );
			( *( __typeof__( fn_save ) )tmp ).d = FN;
			( *( __typeof__( fn_save ) )tmp ).h = hash;
			( *( __typeof__( fn_save ) )tmp ).next = fn_save;
			fn_save = tmp;
			ret.len++;
		}
	}

	ret.fns = malloc( sizeof( ret.fns ) * ret.len );
	i = ret.len;
	while( i-- ){
		tmp = ( *fn_save ).next;
		*( ret.fns + i ) = ( *fn_save ).d;
		for ( ii = 0; ii < ( **( ret.fns + i ) ).fns; ii++ ){
			tmp = BST_search( root, *( ( **( ret.fns + i ) ).hashs + ii ) );
			if ( tmp ){
				*( ( **( ret.fns + i ) ).hashs + ii ) = tmp;
			}else{
				for ( ; i < ret.len; i++ ){
					free( ( **( ret.fns + i ) ).main );
					table_free( &( **( ret.fns + i ) ).table );
					free( ( **( ret.fns + i ) ).hashs );
				}
				free( ret.fns );
				ret.len = 0;
				ret.fns = NULL;
				PERROR( HOMOBF_FN_NOT_FOUND );
				goto RET;
			}
		}
		free( fn_save );
	}

RET:
	while( stack ){
		tmp = ( *stack ).next;
		free( ( *stack ).data );
		free( ( *stack ).cnt );
		free( stack );
		stack = tmp;
	}
	while( fns ){
		tmp = ( *fns ).next;
		free( fns );
		fns = tmp;
	}
	while( fn_save ){
		free( ( *( struct _FUNC * )( *fn_save ).d ).main );
		table_free( &( *( struct _FUNC * )( *fn_save ).d ).table );
		free( ( *( struct _FUNC * )( *fn_save ).d ).hashs );

		tmp = ( *fn_save ).next;
		free( fn_save );
		fn_save = tmp;
	}
	BST_free( root );
	free( mem );
	if ( err ){
		exit( err );
	}
	return ret;
}

char *get_dir ( char *restrict path ){
	register char *ret = NULL;
	register size_t len;
	register size_t i;

	if ( !path ){
		return NULL;
	}

	len = strlen ( path );
	for ( i = len; *( i + path ) - '/' && i--; ){}
	len = i;
	ret = malloc( len + 1 );
	if ( len ){
		memcpy( ret, path, len );
	}
	*( ret + len ) = 0;

	return ret;
}

char *join_path ( char *restrict dir, char *restrict file ){
	register char *ret = NULL;
	register size_t lend, lenf;

	lend = 0;
	lenf = 0;
	if ( dir ){
		lend = strlen( dir );
	}
	if ( file ){
		lenf = strlen( file );
	}

	ret = malloc( lend + lenf + 1 );
	if ( lend ){
		memcpy( ret, dir, lend );
	}
	if ( lenf ){
		memcpy( ret + lend, file, lenf );
	}
	*( ret + lend + lenf ) = 0;

	return ret;
}
