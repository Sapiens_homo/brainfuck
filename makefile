CC=gcc
CFLAGS=-Wall -march=native -std=gnu99
CFLAGS_DBG=$(CFLAGS) -g -D DBG
C_FILES=$(shell find src -name "*.c" )
ASM_SRC=$(shell find src -name "*.s" )
OBJ=$(patsubst %.c, %.c.o, $(C_FILES) )
OBJ_ASM=$(patsubst %.s, %.s.o, $(ASM_SRC) )
OBJ_DBG=$(patsubst %.c, %.c.o.dbg, $(C_FILES) )
OBJ_ASM_DBG=$(patsubst %.s, %.s.o.dbg, $(ASM_SRC) )
OBJ_FILE=$(shell find . -name "*.o*" )
prefix=/opt/homobf
OUTPUT=homobf

.PHONY: all debug clean install uninstall
all: $(OUTPUT)
install: all
	@echo Installing to $(prefix)
	@mkdir -p $(prefix)/bin
	@cp $(OUTPUT) $(prefix)/bin
	@cp README.md $(prefix)
	@chmod 755 src/*.sh
	@chmod 755 src/*.csh
	@cp src/homobf.sh /etc/profile.d
	@cp src/homobf.csh /etc/profile.d
	@echo done
uninstall:
	@echo Uninstalling form $(prefix)
	@-rm -r $(prefix)
	@-rm /etc/profile.d/homobf.csh
	@-rm /etc/profile.d/homobf.sh
	@echo done
debug: $(OUTPUT).dbg
clean:
	@echo Cleaning...
	@-rm -f $(OBJ_FILE)
	@-rm $(OUTPUT)*
	@echo Done
%.c.o: %.c
	@echo Compiling and generating object $@
	@$(CC) $(CFLAGS) -c -o $@ $<
%.s.o: %.s
	@echo Assembling and generating object $@
	@$(CC) -c -o $@ $<
%.c.o.dbg: %.c
	@echo Compiling and generating debugging object $@
	@$(CC) $(CFLAGS_DBG) -c -o $@ $<
%.s.o.dbg: %.s
	@echo Assembling and generating debugging object $@
	@$(CC) -g -c -o $@ $<
$(OUTPUT): $(OBJ) $(OBJ_ASM)
	@echo Linking executable $@
	@$(MAKE) -C hcl
	@$(CC) $(CFLAGS) -Lhcl -lhcl -o $@ $^
$(OUTPUT).dbg: $(OBJ_DBG) $(OBJ_ASM_DBG)
	@echo Linking executable $@
	@$(MAKE) -C hcl debug
	@$(CC) $(CFLAGS_DBG) -Lhcl -lhcldbg -o $@ $^
